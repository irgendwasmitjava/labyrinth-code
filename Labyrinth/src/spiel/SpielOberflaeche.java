package spiel;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.Timer;
import sprite.Konstanten;
import sprite.Pictures;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Name
 */
public class SpielOberflaeche extends javax.swing.JPanel {

    Steuerung dieSteuerung;
    Timer t;
    Pictures p;
    public SpielOberflaeche(String mapName) {
        initComponents();
        this.setFocusable(true);
        p = new Pictures();
        
        dieSteuerung = new Steuerung(mapName);
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                repaint();
                t.start();
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_S:
                        dieSteuerung.derSpieler.aendereAusrichtung(Konstanten.AUSRICHTUNG_UNTEN);
                        break;
                    case KeyEvent.VK_W:
                        dieSteuerung.derSpieler.aendereAusrichtung(Konstanten.AUSRICHTUNG_OBEN);
                        break;
                    case KeyEvent.VK_A:
                        dieSteuerung.derSpieler.aendereAusrichtung(Konstanten.AUSRICHTUNG_LINKS);
                        break;
                    case KeyEvent.VK_D:
                        dieSteuerung.derSpieler.aendereAusrichtung(Konstanten.AUSRICHTUNG_RECHTS);
                        break;
                }
            }
            public void keyReleased(KeyEvent evt){
                t.stop();
            }
        });
        t = new Timer(15, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dieSteuerung.verarbeiteUhrTick();
                p.repeatAnimation();
                repaint();
            }
        });
    }

    public void paintComponent(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getWidth());
        Graphics2D g2d = (Graphics2D) g;
        dieSteuerung.zeichneAlleObjekte(g2d, SpielFenster.size, p);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
