package spiel;

import java.awt.Graphics2D;
import java.awt.Point;
import sprite.Pictures;
import sprite.Spieler;

public class Steuerung {

    public Spieler derSpieler;
    private Maploader mLoader;
    private Point endPunkt;
    Map dieKarte;
    public Steuerung(String mapName) {
        mLoader = new Maploader();
        dieKarte = mLoader.berechneMap(mapName);
        derSpieler = new Spieler(dieKarte.getStartPunkt().x, dieKarte.getStartPunkt().y, dieKarte.getWorldArray());
        endPunkt = dieKarte.getEndPunkt();
        dieKarte.derSchluessel.Spieler = derSpieler;
    }
    public void initObjects(int pSize){
        derSpieler.initialisiereSpieler(pSize);
    }
    public void zeichneAlleObjekte(Graphics2D g2d, int size, Pictures p) {
        // dieKarte.zeichne(g2d, size);
        dieKarte.zeichneAdvanced(g2d, size, derSpieler.getAusrichtung(), derSpieler.getIndex(derSpieler.getPosition().x, derSpieler.getPosition().y), p);
        derSpieler.zeichne(g2d, size, p);
    }

    public void verarbeiteUhrTick() {
        Point p = derSpieler.getIndex(derSpieler.getPosition().x, derSpieler.getPosition().y);

        if (dieKarte.getSchluesselPunkt().x == p.x && dieKarte.getSchluesselPunkt().y == p.y) {
            derSpieler.setzeKey();
            System.out.println("hat key aufgehoben");
        }
        if (derSpieler.gibKey() == true && p.x == endPunkt.x && p.y == endPunkt.y) {
            System.out.println("You win");
        }
        derSpieler.bewege();
    }
}
