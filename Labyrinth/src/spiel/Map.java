package spiel; 

import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;
import sprite.Konstanten;
import sprite.Pictures;
import sprite.Schluessel;
import sprite.Sprite;


public class Map {

    private Sprite[][] map;
    private Point schluesselPunkt;
    private Point startPunkt;
    private Point endPunkt;
    public Schluessel derSchluessel;

    private ArrayList<Point> schluesselPunkte = new ArrayList<>();

    private Random wuerfel = new Random();

    public Map(Sprite[][] pWorldArray) {
        map = pWorldArray;
        berechneWichtigePunkte();
        berechneSchluesselPunkt();
        derSchluessel = new Schluessel(schluesselPunkt.x, schluesselPunkt.y);

    }

    private void berechneWichtigePunkte() {
        for (int y = 0; y < map[0].length; y++) {
            for (int x = 0; x < map.length; x++) {
                if (map[x][y].getType() == Konstanten.TYPE_SCHLUESSELPUNKT) {
                    schluesselPunkte.add(new Point(map[x][y].getPosition().x, map[x][y].getPosition().y));
                }
                if (map[x][y].getType() == Konstanten.TYPE_STARTPUNKT) {
                    startPunkt = new Point(map[x][y].getPosition());
                }
                if (map[x][y].getType() == Konstanten.TYPE_TUER) {
                    endPunkt = new Point(map[x][y].getPosition());
                }
            }
        }
    }

    private void berechneSchluesselPunkt() {
        int index = wuerfel.nextInt(schluesselPunkte.size());
        schluesselPunkt = schluesselPunkte.get(index);
    }

    public Point getSchluesselPunkt() {
        return schluesselPunkt;
    }

    public Point getStartPunkt() {
        return startPunkt;
    }

    public Point getEndPunkt() {
        return endPunkt;
    }

    public Sprite[][] getWorldArray() {
        return map;
    }

//    public void zeichne(Graphics2D g2d, int size) {
//        for (int y = 0; y < map[0].length; y++) {
//            for (int x = 0; x < map.length; x++) {
//                map[x][y].zeichne(g2d, size);
//            }
//        }
//    }
    public int getSize(){
        return map.length;
    }
    public void zeichneAdvanced(Graphics2D g2d, int size, int pAusrichtung, Point pPlayerPosition, Pictures p) {
        switch (pAusrichtung) {
            case Konstanten.AUSRICHTUNG_OBEN:
                for (int y = 0; y >= -4; y--) {
                    for (int x = -1; x <= 1; x++) {
                        if (pPlayerPosition.y + y >= 0) {
                            // Falls in der Mitte eine Wand ist und links / rechts ein Weg ist wird dieser nicht gezeichnet
                            if (x == -1
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() != Konstanten.TYPE_WAND
                                    || x == 1
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() != Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Falls in der Mitte eine Wand ist und links / rechts auch eine Wand ist wird diese gezeichnet
                            if (x == -1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    || x == 1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Blick gerade aus
                            if (x == 0) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);

                            }
                            // Falls der Schluessel in der Sichtweite ist wird er gezeichnet
                            if (derSchluessel.hatGleicheXY(pPlayerPosition.x + x, pPlayerPosition.y + y) 
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() != Konstanten.TYPE_WAND
                                    ) {
                                derSchluessel.zeichne(g2d, size, p);
                            }

                        }
                    }

                    // Falls Sicht ausserhalb der Map gehen wuerde
                    if (pPlayerPosition.y + y >= 0) {
                        if (map[pPlayerPosition.x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND) {
                            break;
                        }
                    }

                }
                break;
            case Konstanten.AUSRICHTUNG_UNTEN:
                for (int y = 0; y <= 4; y++) {
                    for (int x = -1; x <= 1; x++) {
                        if (pPlayerPosition.y + y < map[0].length) {
                            // Falls in der Mitte eine Wand ist und links / rechts ein Weg ist wird dieser nicht gezeichnet
                            if (x == -1
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() != Konstanten.TYPE_WAND
                                    || x == 1
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() != Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Falls in der Mitte eine Wand ist und links / rechts auch eine Wand ist wird diese gezeichnet
                            if (x == -1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    || x == 1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Blick geradeaus
                            if (x == 0) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Falls der Schluessel in der Sichtweite ist wird er gezeichnet
                            if (derSchluessel.hatGleicheXY(pPlayerPosition.x + x, pPlayerPosition.y + y)
                                    && map[pPlayerPosition.x][pPlayerPosition.y + y].getType() != Konstanten.TYPE_WAND) {
                                derSchluessel.zeichne(g2d, size, p);
                            }
                        }
                    }
                    // Falls Sicht ausserhalb der Map gehen wuerde
                    if (map[pPlayerPosition.x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND) {
                        break;
                    }

                }
                break;

            case Konstanten.AUSRICHTUNG_LINKS:
                for (int x = 0; x >= -4; x--) {
                    for (int y = - 1; y <= 1; y++) {
                        if (pPlayerPosition.x + x >= 0) {
                            // Falls in der Mitte eine Wand ist und links / rechts ein Weg ist wird dieser nicht gezeichnet
                            if (y == -1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() != Konstanten.TYPE_WAND
                                    || y == 1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() != Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Falls in der Mitte eine Wand ist und links / rechts auch eine Wand ist wird diese gezeichnet
                            if (y == -1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() == Konstanten.TYPE_WAND
                                    || y == 1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() == Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Blick geradeaus
                            if (y == 0) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Falls der Schluessel in der Sichtweite ist wird er gezeichnet
                            if (derSchluessel.hatGleicheXY(pPlayerPosition.x + x, pPlayerPosition.y + y)
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() != Konstanten.TYPE_WAND) {
                                derSchluessel.zeichne(g2d, size, p);
                            }
                        }
                    }
                    // Falls Sicht ausserhalb der Map gehen wuerde
                    if (pPlayerPosition.x + x >= 0) {
                        if (map[pPlayerPosition.x + x][pPlayerPosition.y].getType() == Konstanten.TYPE_WAND) {
                            break;
                        }
                    }

                }
                break;

            case Konstanten.AUSRICHTUNG_RECHTS:
                for (int x = 0; x <= 4; x++) {
                    for (int y = -1; y <= 1; y++) {
                        if (pPlayerPosition.x + x < map.length) {
                            // Falls in der Mitte eine Wand ist und links / rechts ein Weg ist wird dieser nicht gezeichnet
                            if (y == -1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() != Konstanten.TYPE_WAND
                                    || y == 1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() != Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Falls in der Mitte eine Wand ist und links / rechts auch eine Wand ist wird diese gezeichnet
                            if (y == -1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() == Konstanten.TYPE_WAND
                                    || y == 1
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y + y].getType() == Konstanten.TYPE_WAND
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() == Konstanten.TYPE_WAND) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Blick geradeaus
                            if (y == 0) {
                                map[pPlayerPosition.x + x][pPlayerPosition.y + y].zeichne(g2d, size, p);
                            }
                            // Falls der Schluessel in der Sichtweite ist wird er gezeichnet
                            if (derSchluessel.hatGleicheXY(pPlayerPosition.x + x, pPlayerPosition.y + y)
                                    && map[pPlayerPosition.x + x][pPlayerPosition.y].getType() != Konstanten.TYPE_WAND) {
                                derSchluessel.zeichne(g2d, size, p);
                            }
                        }
                    }
                    // Falls Sicht ausserhalb der Map gehen wuerde
                    if (map[pPlayerPosition.x + x][pPlayerPosition.y].getType() == Konstanten.TYPE_WAND) {
                        break;
                    }

                }
                break;

        }
    }
}
