package spiel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import sprite.Konstanten;
import sprite.Sprite;
import sprite.Tuer;
import sprite.Wand;
import sprite.Weg;

public class Maploader {

    private BufferedReader reader;
    private ArrayList<String> stringListe = new ArrayList<>();
    private String[][] stringArray;
    private String[][] vergleichArray;
    private Sprite[][] worldArray;
    private Map dieKarte;

    public Map berechneMap(String file) {
        openFile(file);
        createStringListe();
        createStringArray();
        createSpriteArray();
        closeFile();
        dieKarte = new Map(worldArray);
        return dieKarte;
    }

    private void openFile(String fileName) {
        Properties props = System.getProperties();
        try {
            reader = new BufferedReader(new FileReader(props.getProperty("user.dir") + File.separator + "levels" + File.separator + fileName + ".txt"));
        } catch (Exception e) {
            System.out.println("oops there is a bug" + e.getMessage());
        }
    }

    private void closeFile() {
        try {
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(Maploader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void createStringListe() {
        String i;
        try {
            while ((i = reader.readLine()) != null) {
                stringListe.add(i);
            }
        } catch (IOException ex) {
            System.out.println("bug with reading file");
        }
    }

    private void createStringArray() {
        stringArray = new String[stringListe.get(0).length()][stringListe.size()];
        for (int y = 0; y < stringArray[0].length; y++) {
            String[] row = stringListe.get(y).split("");
            for (int x = 0; x < stringArray.length; x++) {
                stringArray[x][y] = row[x];
            }
        }
        createVergleichArray();
    }

    private void createVergleichArray() {
        vergleichArray = new String[stringArray.length + 2][stringArray[0].length + 2];
        for (int y = 0; y < vergleichArray[0].length; y++) {
            for (int x = 0; x < vergleichArray.length; x++) {
                vergleichArray[x][y] = "a";
            }
        }
        for (int y = 0; y < stringArray[0].length; y++) {
            for (int x = 0; x < stringArray.length; x++) {
                vergleichArray[x + 1][y + 1] = stringArray[x][y];
            }
        }
    }

    // Nur zu Testzwecken
    public int[][] getArray() {
        int[][] array = new int[stringArray.length][stringArray[0].length];
        for (int y = 0; y < array[0].length; y++) {
            for (int x = 0; x < array.length; x++) {
                switch (stringArray[x][y]) {
                    case "a":
                        array[x][y] = 1;
                        break;
                    case "b":
                        array[x][y] = 2;
                        break;
                }
            }
        }
        return array;
    }
    int itterations;

    private int getType(int pX, int pY) {
        int type = Konstanten.TYPE_WEG;
        //Wand Säule
        if (!"a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])) {
            type = Konstanten.TYPE_WAND_SÄULE;
        }  
        // Wand senkrecht linke seite
        if ("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX ][pY])&& "a".equals(vergleichArray[pX][pY + 2])) {
            type = Konstanten.TYPE_WAND_SENKRECHT_LINKE_SEITE;
        }
        // Wand senkrecht rechte seite
        if ("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX + 2][pY])&& "a".equals(vergleichArray[pX + 2][pY + 2])) {
            type = Konstanten.TYPE_WAND_SENKRECHT_RECHTE_SEITE;
        }
        //Wand senkrecht 
        if("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])){
//                    System.out.println("hi");
            itterations++;
            type = Konstanten.TYPE_WAND_SENKRECHT;
        }
        //Wand waagerecht obere seite
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX ][pY])&& "a".equals(vergleichArray[pX + 2][pY])){
            type = Konstanten.TYPE_WAND_WAAGERECHT_OBEN_SEITE;
        }
        //Wand waagerecht
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])){
            type = Konstanten.TYPE_WAND_WAAGERECHT;
        }
        //Wand waagerecht untere Seite
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX ][pY +2])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_WAAGERECHT_UNTEN_SEITE;
        }
        //Wand Uturn oben
        if(!"a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])){
            type = Konstanten.TYPE_WAND_U_TURN_OBEN;
        }
        //Wand Uturn unten
        if("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])){
            type = Konstanten.TYPE_WAND_U_TURN_UNTEN;
        }
        //Wand Uturn rechts
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])){
            type = Konstanten.TYPE_WAND_U_TURN_RECHTS;
        }
        //Wand Uturn links
        if(!"a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])){
            type = Konstanten.TYPE_WAND_U_TURN_LINKS;
        }
        // Wand Links Unten
        if("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX + 2][pY])){
            type = Konstanten.TYPE_WAND_ECKE_LINKS_UNTEN;
        }
        //Wand Rechts Unten
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY])){
            type = Konstanten.TYPE_WAND_ECKE_RECHTS_UNTEN;
        }
        //Wand Rechts Oben
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY + 2])){
            type = Konstanten.TYPE_WAND_ECKE_RECHTS_OBEN;
        }
        //Wand Links Oben
        if(!"a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_ECKE_LINKS_OBEN;
        }
        //Wand Rechts Oben mit Innen
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY + 2])){
            type = Konstanten.TYPE_WAND_RECHTS_OBEN_MIT_INNEN;
        }
        //Wand Rechts Unten mit Innen
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])){
            type = Konstanten.TYPE_WAND_RECHTS_UNTEN_MIT_INNEN;
        }
        //Wand Links Oben mit Innen
        if(!"a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_LINKS_OBEN_MIT_INNEN;
        }
        //Wand Links Unten mit Innen
        if("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX + 2][pY])){
            type = Konstanten.TYPE_WAND_LINKS_UNTEN_MIT_INNEN;
        }
        //Wand Rechts Oben nur Innen
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX ][pY + 2])&& "a".equals(vergleichArray[pX][pY])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_RECHTS_OBEN_NUR_INNEN;
        }
        //Wand Rechts Unten nur Innen
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX + 2][pY + 2])&& !"a".equals(vergleichArray[pX ][pY])&& "a".equals(vergleichArray[pX][pY + 2])&& "a".equals(vergleichArray[pX + 2][pY])){
            type = Konstanten.TYPE_WAND_RECHTS_UNTEN_NUR_INNEN;
        }
        //Wand Links Oben nur Innen
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2 ][pY + 2])&& "a".equals(vergleichArray[pX][pY + 2])&& "a".equals(vergleichArray[pX + 2][pY])){
            type = Konstanten.TYPE_WAND_LINKS_OBEN_NUR_INNEN;
        }
        //Wand Links Unten nur Innen
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY])&& "a".equals(vergleichArray[pX][pY])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_LINKS_UNTEN_NUR_INNEN;
        }
        //Wand Innen
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY])&& "a".equals(vergleichArray[pX + 2][pY])&& "a".equals(vergleichArray[pX][pY + 2])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN;
        }
        //Wand Innen Kreuz
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ;
        }
        //Wand Innen Kreuz rechts
        if("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_RECHTS;
        }
        //Wand Innen Kreuz links
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_LINKS;
        }
        //Wand Innen Kreuz unten
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_UNTEN;
        }
        //Wand Innen Kreuz oben
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_OBEN;
        }
        //Wand Innen Kreuz rechts Wand
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& "a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_RECHTS_WAND;
        }
        //Wand Innen Kreuz links Wand
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& "a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX][pY + 2])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_LINKS_WAND;
        }
        //Wand Innen Kreuz unten Wand
         if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY])&& "a".equals(vergleichArray[pX + 2][pY])&& !"a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_UNTEN_WAND;
        }
        //Wand Innen Kreuz oben Wand
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& "a".equals(vergleichArray[pX][pY + 2])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_INNEN_KREUZ_OBEN_WAND;
        }
        //Wand Abzweig RECHTS rechts
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY])&& !"a".equals(vergleichArray[pX + 2][pY])&& (!"a".equals(vergleichArray[pX][pY + 2])||"a".equals(vergleichArray[pX][pY + 2]))&& (!"a".equals(vergleichArray[pX + 2][pY + 2])||"a".equals(vergleichArray[pX + 2][pY + 2]))){
            type = Konstanten.TYPE_WAND_ABZWEIG_RECHTS_RECHTS;
        }
        //Wand Abzweig RECHTS links
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& (!"a".equals(vergleichArray[pX][pY])||"a".equals(vergleichArray[pX][pY]))&& (!"a".equals(vergleichArray[pX + 2][pY])||"a".equals(vergleichArray[pX + 2][pY]))&& !"a".equals(vergleichArray[pX][pY + 2])&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_ABZWEIG_RECHTS_LINKS;
        }
        //Wand Abzweig RECHTS unten
         if("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& (!"a".equals(vergleichArray[pX][pY])||"a".equals(vergleichArray[pX][pY]))&& "a".equals(vergleichArray[pX + 2][pY])&& (!"a".equals(vergleichArray[pX][pY + 2])||"a".equals(vergleichArray[pX][pY + 2]))&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_ABZWEIG_RECHTS_UNTEN;
        }
        //Wand Abzweig RECHTS oben
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& (!"a".equals(vergleichArray[pX + 2][pY])||"a".equals(vergleichArray[pX + 2][pY]))&& "a".equals(vergleichArray[pX][pY + 2])&& (!"a".equals(vergleichArray[pX + 2][pY + 2])||"a".equals(vergleichArray[pX + 2][pY + 2]))){
            type = Konstanten.TYPE_WAND_ABZWEIG_RECHTS_OBEN;
        }
        //Wand Abzweig LINKS rechts
        if(!"a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& (!"a".equals(vergleichArray[pX][pY])||"a".equals(vergleichArray[pX][pY]))&& (!"a".equals(vergleichArray[pX + 2][pY])||"a".equals(vergleichArray[pX + 2][pY]))&& "a".equals(vergleichArray[pX][pY + 2])&& !"a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_ABZWEIG_LINKS_RECHTS;
        }
        //Wand Abzweig LINKS links
        if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && !"a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& !"a".equals(vergleichArray[pX][pY])&& "a".equals(vergleichArray[pX + 2][pY])&& (!"a".equals(vergleichArray[pX][pY + 2])||"a".equals(vergleichArray[pX][pY + 2]))&& (!"a".equals(vergleichArray[pX + 2][pY + 2])||"a".equals(vergleichArray[pX + 2][pY + 2]))){
            type = Konstanten.TYPE_WAND_ABZWEIG_LINKS_LINKS;
        }
        //Wand Abzweig LINKS unten
         if("a".equals(vergleichArray[pX + 1][pY]) && "a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && !"a".equals(vergleichArray[pX + 2][pY + 1])&& "a".equals(vergleichArray[pX][pY])&& (!"a".equals(vergleichArray[pX + 2][pY])||"a".equals(vergleichArray[pX + 2][pY]))&& !"a".equals(vergleichArray[pX][pY + 2])&& (!"a".equals(vergleichArray[pX + 2][pY + 2])||"a".equals(vergleichArray[pX + 2][pY + 2]))){
            type = Konstanten.TYPE_WAND_ABZWEIG_LINKS_UNTEN;
        }
        //Wand Abzweig LINKS oben
        if("a".equals(vergleichArray[pX + 1][pY]) && !"a".equals(vergleichArray[pX][pY + 1]) && "a".equals(vergleichArray[pX + 1][pY + 2]) && "a".equals(vergleichArray[pX + 2][pY + 1])&& (!"a".equals(vergleichArray[pX][pY])||"a".equals(vergleichArray[pX][pY]))&& !"a".equals(vergleichArray[pX + 2][pY])&& (!"a".equals(vergleichArray[pX][pY + 2])||"a".equals(vergleichArray[pX][pY + 2]))&& "a".equals(vergleichArray[pX + 2][pY + 2])){
            type = Konstanten.TYPE_WAND_ABZWEIG_LINKS_OBEN;
        }
        
        
        // if(vergleichArray[pX + 1][pY] == "a" && vergleichArray[pX][pY + 1] == "a" && vergleichArray[pX + 1][pY + 2] == "a" && vergleichArray[pX + 2][pY + 1] == "a"){}
//        for (int y = 0; y < vergleichArray[0].length; y++) {
//            for (int x = 0; x < vergleichArray.length; x++) {
//                System.out.print(vergleichArray[x][y]);
//            }
//            System.out.println("");
//        }
//        System.out.println("  ");
        return type;
    }

    public void createSpriteArray() {
        worldArray = new Sprite[stringArray.length][stringArray[0].length];
        for (int y = 0; y < worldArray[0].length; y++) {
            for (int x = 0; x < worldArray.length; x++) {
                switch (stringArray[x][y]) {
                    case "a":
                        int type = getType(x, y);
                        worldArray[x][y] = new Wand(x, y, Konstanten.TYPE_WAND, type);
                        break;
                    case "b":
                        worldArray[x][y] = new Weg(x, y, Konstanten.TYPE_WEG);
                        break;
                    case "c":
                        worldArray[x][y] = new Weg(x, y, Konstanten.TYPE_SCHLUESSELPUNKT);
                        break;
                    case "d":
                        worldArray[x][y] = new Weg(x, y, Konstanten.TYPE_STARTPUNKT);
                        break;
                    case "e":
                        worldArray[x][y] = new Tuer(x, y, Konstanten.TYPE_TUER);
                        break;
                }
            }
        }
        System.out.println(itterations);
    }

}
