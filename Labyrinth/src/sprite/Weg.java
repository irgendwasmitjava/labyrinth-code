package sprite;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;


public class Weg extends Sprite {

    // Normaler Weg
    public Weg(int x, int y, int pType) {
        super(x, y);
        type = pType;
    }

    @Override
    public BufferedImage gibAktuellesBild() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void zeichne(Graphics g2d, int size, Pictures p) {
        g2d.setColor(Color.WHITE);
//        g2d.fillRect(position.x * size, position.y * size, size, size);
        g2d.drawImage(p.Flur, position.x* size, position.y * size, size, size, null);
    }

}
