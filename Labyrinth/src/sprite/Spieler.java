package sprite;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


public class Spieler extends Figur {

    private boolean hatKey;
    private Sprite[][] weltArray;
    private int schrittweite = 2;
    private int range;
    private int spielerSize = 20;
    private int size;
    public Spieler(int x, int y, Sprite[][] pWeltArray) {
        super(x, y);
        weltArray = pWeltArray;
        hatKey = false;

    }

    public void initialisiereSpieler(int pSize) {
        position.x *= pSize + 1;
        position.y *= pSize + 1;
        range = pSize -spielerSize;
        size = pSize;
    }

    public void setzeKey() {
        hatKey = true;
    }

    @Override
    public void bewege() {
        Point index = getIndex(position.x, position.y);
        Point nextPoint;
        Point anotherNextPoint;
        switch (ausrichtung) {
            case Konstanten.AUSRICHTUNG_OBEN:
                nextPoint = getIndex(position.x, position.y - schrittweite);
                anotherNextPoint = getIndex(position.x + spielerSize - 1, position.y - schrittweite);
                if (position.y - schrittweite >= 0 && weltArray[nextPoint.x][nextPoint.y].getType() != Konstanten.TYPE_WAND && weltArray[anotherNextPoint.x][anotherNextPoint.y].getType() != Konstanten.TYPE_WAND) {
                    position.y -= schrittweite;
                }
                break;
            case Konstanten.AUSRICHTUNG_RECHTS:
                nextPoint = getIndex(position.x + spielerSize + schrittweite, position.y);
                anotherNextPoint = getIndex(position.x + spielerSize + schrittweite, position.y + spielerSize - 1);
                if (position.x + schrittweite <= index.x * size + range || weltArray[nextPoint.x][nextPoint.y].getType() != Konstanten.TYPE_WAND && weltArray[anotherNextPoint.x][anotherNextPoint.y].getType() != Konstanten.TYPE_WAND) {
                    position.x += schrittweite;
                }
                break;
            case Konstanten.AUSRICHTUNG_UNTEN:
                nextPoint = getIndex(position.x, position.y + spielerSize + schrittweite);
                anotherNextPoint = getIndex(position.x + spielerSize - 1, position.y + spielerSize + schrittweite);
                if (position.y + schrittweite <= index.y * size + range || weltArray[nextPoint.x][nextPoint.y].getType() != Konstanten.TYPE_WAND && weltArray[anotherNextPoint.x][anotherNextPoint.y].getType() != Konstanten.TYPE_WAND) {
                    position.y += schrittweite;
                }
                break;
            case Konstanten.AUSRICHTUNG_LINKS:
                nextPoint = getIndex(position.x - schrittweite, position.y);
                anotherNextPoint = getIndex(position.x - schrittweite, position.y + spielerSize - 1);
                if (position.x - schrittweite >= 0 && weltArray[nextPoint.x][nextPoint.y].getType() != Konstanten.TYPE_WAND && weltArray[anotherNextPoint.x][anotherNextPoint.y].getType() != Konstanten.TYPE_WAND) {
                    position.x -= schrittweite;
                }
                break;
        }
    }

    @Override
    public void aendereAusrichtung(int pAusrichtung) {
        ausrichtung = pAusrichtung;
    }

    @Override
    public void zeichne(Graphics g2d, int size, Pictures p) {
        g2d.setColor(Color.RED);
//        g2d.fillRect(position.x, position.y, spielerSize, spielerSize);
        if(ausrichtung == Konstanten.AUSRICHTUNG_UNTEN){
              g2d.drawImage(p.SpielerVorne[p.currentPic[0]], position.x, position.y, spielerSize,spielerSize,null);
        }else if(ausrichtung == Konstanten.AUSRICHTUNG_OBEN){
              g2d.drawImage(p.SpielerHinten[p.currentPic[0]], position.x, position.y, spielerSize,spielerSize,null);
        } else if(ausrichtung == Konstanten.AUSRICHTUNG_RECHTS){
              g2d.drawImage(p.SpielerRechts[p.currentPic[1]], position.x, position.y, spielerSize,spielerSize,null);
        } else if(ausrichtung == Konstanten.AUSRICHTUNG_LINKS){
              g2d.drawImage(p.SpielerLinks[p.currentPic[1]], position.x, position.y, spielerSize,spielerSize,null);
        }
    }

    public Point getIndex(int pX, int pY) {
        Point p = new Point();
        p.x = (int) pX / size;
        p.y = (int) pY / size;
        return p;
    }

    public boolean gibKey() {
        return hatKey;
    }

    public int getAusrichtung() {
        return ausrichtung;
    }
}
