package sprite;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Wand extends Sprite {
    int wandType;
    public Wand(int x, int y, int pType, int pWandType) {
        super(x, y);
        type = pType;
        wandType = pWandType;
    }

    @Override
    public BufferedImage gibAktuellesBild() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void zeichne(Graphics g2d, int size, Pictures p) {
        g2d.setColor(Color.BLUE);
        switch (wandType) {
            case Konstanten.TYPE_WAND_SENKRECHT_LINKE_SEITE:
                g2d.drawImage(p.wallYLW, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_SENKRECHT_RECHTE_SEITE:
                g2d.drawImage(p.wallYRW, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_SENKRECHT: 
                g2d.drawImage(p.wallY, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_WAAGERECHT_OBEN_SEITE:
                g2d.drawImage(p.wallXUW, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_U_TURN_OBEN:
                g2d.drawImage(p.wallUturnUp, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_U_TURN_UNTEN:
                g2d.drawImage(p.wallUturnDown, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_WAAGERECHT:
                g2d.drawImage(p.wallX, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_SÄULE:
                g2d.drawImage(p.wallPillar, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_U_TURN_RECHTS:
                g2d.drawImage(p.wallUturnRight, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_U_TURN_LINKS:
                g2d.drawImage(p.wallUturnLeft, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_WAAGERECHT_UNTEN_SEITE:
                g2d.drawImage(p.wallXDW, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ECKE_LINKS_UNTEN:
                g2d.drawImage(p.wallLeftDown, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ECKE_RECHTS_UNTEN:
                g2d.drawImage(p.wallRightDown, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ECKE_RECHTS_OBEN:
                g2d.drawImage(p.wallRightUp, size * position.x, size * position.y, size, size, null);
                break;  
            case Konstanten.TYPE_WAND_ECKE_LINKS_OBEN:
                g2d.drawImage(p.wallLeftUp, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_RECHTS_OBEN_MIT_INNEN:
                g2d.drawImage(p.wallRightUpIn, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_RECHTS_UNTEN_MIT_INNEN:
                g2d.drawImage(p.wallRightDownIn, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_LINKS_OBEN_MIT_INNEN:
                g2d.drawImage(p.wallLeftUpIn, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_LINKS_UNTEN_MIT_INNEN:
                g2d.drawImage(p.wallLeftDownIn, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_RECHTS_OBEN_NUR_INNEN:
                g2d.drawImage(p.wallRightUpOnlyIn, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_RECHTS_UNTEN_NUR_INNEN:
                g2d.drawImage(p.wallRightDownOnlyIn, size * position.x, size * position.y, size, size, null);
                break; 
            case Konstanten.TYPE_WAND_LINKS_OBEN_NUR_INNEN:
                g2d.drawImage(p.wallLeftUpOnlyIn, size * position.x, size * position.y, size, size, null);
                break; 
            case Konstanten.TYPE_WAND_LINKS_UNTEN_NUR_INNEN:
                g2d.drawImage(p.wallLeftDownOnlyIn, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN:
                g2d.drawImage(p.wallIn, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ:
                g2d.drawImage(p.wallCross, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ_UNTEN:
                g2d.drawImage(p.wallCrossDown, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ_OBEN:
                g2d.drawImage(p.wallCrossUp, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ_LINKS:
                g2d.drawImage(p.wallCrossLeft, size * position.x, size * position.y, size, size, null);
                break;    
            case Konstanten.TYPE_WAND_INNEN_KREUZ_RECHTS:
                g2d.drawImage(p.wallCrossRight, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ_RECHTS_WAND:
                g2d.drawImage(p.wallCrossRightWall, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ_LINKS_WAND:
                g2d.drawImage(p.wallCrossLeftWall, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ_UNTEN_WAND:
                g2d.drawImage(p.wallCrossDownWall, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_INNEN_KREUZ_OBEN_WAND:
                g2d.drawImage(p.wallCrossUpWall, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_RECHTS_OBEN:
                g2d.drawImage(p.wallPathRightUp, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_RECHTS_UNTEN:
                g2d.drawImage(p.wallPathRightDown, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_RECHTS_LINKS:
                g2d.drawImage(p.wallPathRightLeft, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_RECHTS_RECHTS:
                g2d.drawImage(p.wallPathRightRight, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_LINKS_OBEN:
                g2d.drawImage(p.wallPathLeftUp, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_LINKS_UNTEN:
                g2d.drawImage(p.wallPathLeftDown, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_LINKS_LINKS:
                g2d.drawImage(p.wallPathLeftLeft, size * position.x, size * position.y, size, size, null);
                break;
            case Konstanten.TYPE_WAND_ABZWEIG_LINKS_RECHTS:
                g2d.drawImage(p.wallPathLeftRight, size * position.x, size * position.y, size, size, null);
                break;    
                
            default:
//                g2d.fillRect(position.x * size, position.y * size, size, size);
                g2d.drawImage(p.wallPillar, size * position.x, size * position.y, size, size, null);
                break;
        }
    }

}
