/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sprite;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;


public class Pictures {

    public static int currentPic[] = new int[4];
    public int smooth;
    public int SpielerAnimation = 6;
    public int AufhebeAnimation = 6;
    public int LinksRechtsAni = 4;

    public BufferedImage laterne, Tuer, Flur, Key, wallY, wallXUW, wallLeftDown, wallRightDown, wallInnerEdgeUpLeft, wallInnerEdgeDownLeft,
                  wallInnerEdgeUpRight, wallInnerEdgeDownRight, wallYRW, wallYLW, wallUturnLeft, wallUturnRight, wallUturnUp, wallUturnDown, wallX,wallPillar, wallXDW,wallLeftUp,wallRightUp
                  ,wallLeftDownIn,wallLeftUpIn,wallRightDownIn,wallRightUpIn,wallRightUpOnlyIn,wallRightDownOnlyIn,wallLeftUpOnlyIn,wallLeftDownOnlyIn, wallCross,wallCrossDown,wallCrossUp
                  ,wallCrossLeft,wallCrossRight,wallIn,wallPathRightRight,wallPathRightLeft,wallPathRightDown,wallPathRightUp,wallPathLeftRight,wallPathLeftLeft,wallPathLeftDown,wallPathLeftUp
                  ,wallCrossDownWall,wallCrossUpWall,wallCrossLeftWall,wallCrossRightWall;
    public BufferedImage[] SpielerRechts, SpielerLinks, SpielerVorne, SpielerHinten, AufhebenVorne, AufhebenHinten;

    // wallRightToDown, wallLeftToDown,
    
    public Pictures() {
        SpielerVorne = loadPics("res", "images", "spieler", "Lauf_vorne.png", 6, false);
        SpielerHinten = loadPics("res", "images", "spieler", "Lauf_hinten.png", 6, false);
        SpielerRechts = loadPics("res", "images", "spieler", "Laufe_Rechts.png", 4, false);
        SpielerLinks = loadPics("res", "images", "spieler", "Laufe_Links.png", 4, false);
        AufhebenVorne = loadPics("res", "images", "spieler", "aufheben_vorne.png", 6, false);
        AufhebenHinten = loadPics("res", "images", "spieler", "aufheben_hinten.png", 6, false);

        laterne = loadPics("res", "images", "item", "laterne.png", 1, true)[0];
        Tuer = loadPics("res", "images", "item", "Tür.png", 1, true)[0];
        Flur = loadPics("res", "images", "map", "Flur.png", 1, true)[0];
        Key = loadPics("res", "images", "map", "Key.png", 1, true)[0];

        wallY = loadPics("res", "images", "map", "Wand_Senkrecht.png", 1, true)[0];
        wallYRW = loadPics("res", "images", "map", "Wand_Senkrecht_linke_Seite_Mauer.png", 1, true)[0];
        wallYLW = loadPics("res", "images", "map", "Wand_Senkrecht_Rechte_Seite_Mauer.png", 1, true)[0];
        wallX = loadPics("res", "images", "map", "Wand_Wagerecht.png", 1, true)[0];
        wallXUW = loadPics("res", "images", "map", "Wand_Wagerecht_obere_Seite.png", 1, true)[0];
        wallXDW = loadPics("res", "images", "map", "Wand_Wagerecht_untere_Seite.png", 1, true)[0];

//        wallLeftToDown = loadPics("res", "images", "map", "Wand_Ecke_links_nach_unten.png", 1, true)[0];
        wallLeftDown = loadPics("res", "images", "map", "Wand_Ecke_Links_unten.png", 1, true)[0];
//        wallRightToDown = loadPics("res", "images", "map", "Wand_Ecke_rechts_nach_unten.png", 1, true)[0];
        wallRightDown = loadPics("res", "images", "map", "Wand_Ecke_Rechts_unten.png", 1, true)[0];
        wallRightUp = loadPics("res", "images", "map", "Wand_Ecke_Rechts_oben.png", 1, true)[0];
        wallLeftUp = loadPics("res", "images", "map", "Wand_Ecke_Links_oben.png", 1, true)[0];
        wallRightDownIn = loadPics("res", "images", "map", "Wand_Ecke_Rechts_unten_mit_Innen.png", 1, true)[0];
        wallRightUpIn = loadPics("res", "images", "map", "Wand_Ecke_Rechts_oben_mit_Innen.png", 1, true)[0];
        wallLeftDownIn = loadPics("res", "images", "map", "Wand_Ecke_Links_unten_mit_Innen.png", 1, true)[0];
        wallLeftUpIn =  loadPics("res", "images", "map", "Wand_Ecke_Links_oben_mit_Innen.png", 1, true)[0];       
        
        wallRightDownOnlyIn = loadPics("res", "images", "map", "Wand_Ecke_Rechts_unten_nur_Innen.png", 1, true)[0];
        wallRightUpOnlyIn = loadPics("res", "images", "map", "Wand_Ecke_Rechts_oben_nur_Innen.png", 1, true)[0];
        wallLeftDownOnlyIn = loadPics("res", "images", "map", "Wand_Ecke_Links_unten_nur_Innen.png", 1, true)[0]; 
        wallLeftUpOnlyIn = loadPics("res", "images", "map", "Wand_Ecke_Links_oben_nur_Innen.png", 1, true)[0];
        
        
        wallInnerEdgeUpLeft = loadPics("res", "images", "map", "Wand_Innenecke_Links_oben.png", 1, true)[0];
        wallInnerEdgeDownLeft = loadPics("res", "images", "map", "Wand_Innenecke_Links_unten.png", 1, true)[0];
        wallInnerEdgeUpRight = loadPics("res", "images", "map", "Wand_Innenecke_Rechts_oben.png", 1, true)[0];
        wallInnerEdgeDownRight = loadPics("res", "images", "map", "Wand_Innenecke_Rechts_unten.png", 1, true)[0];

        wallUturnLeft = loadPics("res", "images", "map", "Wand_Uturn_Links.png", 1, true)[0];
        wallUturnRight = loadPics("res", "images", "map", "Wand_Uturn_Rechts.png", 1, true)[0];
        wallUturnUp = loadPics("res", "images", "map", "Wand_Uturn_oben.png", 1, true)[0];
        wallUturnDown = loadPics("res", "images", "map", "Wand_Uturn_unten.png", 1, true)[0];
        
        wallPillar = loadPics("res", "images", "map", "Wand_Säule.png", 1, true)[0];
        
        wallCross = loadPics("res", "images", "map", "Wand_Innen_Kreuz.png", 1, true)[0];
        wallCrossDown = loadPics("res", "images", "map", "Wand_Innen_Kreuz_unten.png", 1, true)[0];
        wallCrossLeft = loadPics("res", "images", "map", "Wand_Innen_Kreuz_links.png", 1, true)[0];
        wallCrossRight = loadPics("res", "images", "map", "Wand_Innen_Kreuz_rechts.png", 1, true)[0];
        wallCrossUp = loadPics("res", "images", "map", "Wand_Innen_Kreuz_oben.png", 1, true)[0];
        wallIn = loadPics("res", "images", "map", "Wand_Innen.png", 1, true)[0];
        
        wallCrossDownWall = loadPics("res", "images", "map", "Wand_Innen_Kreuz_unten_Wand.png", 1, true)[0];
        wallCrossUpWall = loadPics("res", "images", "map", "Wand_Innen_Kreuz_oben_Wand.png", 1, true)[0];
        wallCrossLeftWall = loadPics("res", "images", "map", "Wand_Innen_Kreuz_links_Wand.png", 1, true)[0];
        wallCrossRightWall = loadPics("res", "images", "map", "Wand_Innen_Kreuz_rechts_Wand.png", 1, true)[0];
        
        wallPathRightRight = loadPics("res", "images", "map", "Wand_Abzweig_RECHTS_rechts.png", 1, true)[0];
        wallPathRightLeft = loadPics("res", "images", "map", "Wand_Abzweig_RECHTS_links.png", 1, true)[0];
        wallPathRightDown = loadPics("res", "images", "map", "Wand_Abzweig_RECHTS_unten.png", 1, true)[0];
        wallPathRightUp = loadPics("res", "images", "map", "Wand_Abzweig_RECHTS_oben.png", 1, true)[0];
        wallPathLeftRight = loadPics("res", "images", "map", "Wand_Abzweig_LINKS_rechts.png", 1, true)[0];
        wallPathLeftLeft = loadPics("res", "images", "map", "Wand_Abzweig_LINKS_links.png", 1, true)[0];
        wallPathLeftDown = loadPics("res", "images", "map", "Wand_Abzweig_LINKS_unten.png", 1, true)[0];
        wallPathLeftUp = loadPics("res", "images", "map", "Wand_Abzweig_LINKS_oben.png", 1, true)[0];
    }   
    

    public void repeatAnimation() {
            currentPic[0]++;
            if (currentPic[0] >= SpielerAnimation) {
                currentPic[0] = 0;
            }

            currentPic[1]++;
            if (currentPic[1] >= LinksRechtsAni) {
                currentPic[1] = 0;
            }

            currentPic[2]++;
            if (currentPic[2] >= AufhebeAnimation) {
                currentPic[2] = 0;
            }
    }

    private BufferedImage[] loadPics(String path1, String path2, String path3, String path4, int picsLength, boolean format) {

        BufferedImage[] animate = new BufferedImage[picsLength];
        BufferedImage source = null;

        String picFolder1 = path1;
        String picFolder2 = path2;
        String picFolder3 = path3;
        String picFolder4 = path4;

        try {
            source = ImageIO.read(new FileInputStream(picFolder1 + File.separator + picFolder2 + File.separator
                    + picFolder3 + File.separator + picFolder4));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        for (int size = 0; size < picsLength; size++) {
            if (format) {
                animate[size] = source.getSubimage(size * source.getWidth() / picsLength, 0, source.getWidth() / picsLength, source.getHeight());
            } else {
                animate[size] = source.getSubimage(0, size * source.getHeight() / picsLength, source.getWidth(), source.getHeight() / picsLength);
            }
        }
        return animate;
    }
}
