package sprite;


public class Konstanten {

    public static final int TYPE_WEG = 1;
    public static final int TYPE_WAND = 2;
    public static final int TYPE_TUER = 3;
    public static final int TYPE_SCHLUESSELPUNKT = 4;
    public static final int TYPE_STARTPUNKT = 5;
    public static final int TYPE_ENDPUNKT = 6;

    public static final int SIZE = 50;

    public static final int AUSRICHTUNG_OBEN = 1;
    public static final int AUSRICHTUNG_UNTEN = 2;
    public static final int AUSRICHTUNG_LINKS = 3;
    public static final int AUSRICHTUNG_RECHTS = 4;

    public static final int TYPE_WAND_SENKRECHT_LINKE_SEITE = 7;
    public static final int TYPE_WAND_SÄULE = 8;
    public static final int TYPE_WAND_WAAGERECHT = 9;
    public static final int TYPE_WAND_SENKRECHT_RECHTE_SEITE = 10;
    public static final int TYPE_WAND_WAAGERECHT_OBEN_SEITE = 11;
    public static final int TYPE_WAND_SENKRECHT = 12;
    public static final int TYPE_WAND_ECKE_LINKS_UNTEN = 13;
    public static final int TYPE_WAND_ECKE_RECHTS_UNTEN = 14;
    public static final int TYPE_WAND_ECKE_RECHTS_OBEN = 15;
    public static final int TYPE_WAND_ECKE_LINKS_OBEN = 16;
   
    public static final int TYPE_WAND_U_TURN_UNTEN = 17;
    public static final int TYPE_WAND_U_TURN_OBEN = 18;
    public static final int TYPE_WAND_U_TURN_RECHTS = 19;
    public static final int TYPE_WAND_U_TURN_LINKS = 20;
    
    public static final int TYPE_WAND_WAAGERECHT_UNTEN_SEITE = 21;
    public static final int TYPE_WAND_RECHTS_OBEN_MIT_INNEN = 22;
    public static final int TYPE_WAND_RECHTS_UNTEN_MIT_INNEN = 23;
    public static final int TYPE_WAND_LINKS_OBEN_MIT_INNEN = 24;
    public static final int TYPE_WAND_LINKS_UNTEN_MIT_INNEN = 25;
    
    public static final int TYPE_WAND_LINKS_UNTEN_NUR_INNEN = 26;
    public static final int TYPE_WAND_LINKS_OBEN_NUR_INNEN = 27;
    public static final int TYPE_WAND_RECHTS_UNTEN_NUR_INNEN = 28;
    public static final int TYPE_WAND_RECHTS_OBEN_NUR_INNEN = 29;
    
    public static final int TYPE_WAND_INNEN = 30;
    public static final int TYPE_WAND_INNEN_KREUZ = 31;
    public static final int TYPE_WAND_INNEN_KREUZ_RECHTS = 32;
    public static final int TYPE_WAND_INNEN_KREUZ_LINKS = 33;
    public static final int TYPE_WAND_INNEN_KREUZ_OBEN = 34;
    public static final int TYPE_WAND_INNEN_KREUZ_UNTEN = 35;
    
    public static final int TYPE_WAND_INNEN_KREUZ_RECHTS_WAND = 36;
    public static final int TYPE_WAND_INNEN_KREUZ_LINKS_WAND = 37;
    public static final int TYPE_WAND_INNEN_KREUZ_OBEN_WAND = 38;
    public static final int TYPE_WAND_INNEN_KREUZ_UNTEN_WAND = 39;
    
    public static final int TYPE_WAND_ABZWEIG_RECHTS_RECHTS = 40;
    public static final int TYPE_WAND_ABZWEIG_RECHTS_LINKS = 41;
    public static final int TYPE_WAND_ABZWEIG_RECHTS_OBEN = 42;
    public static final int TYPE_WAND_ABZWEIG_RECHTS_UNTEN = 43;
    
    public static final int TYPE_WAND_ABZWEIG_LINKS_RECHTS = 44;
    public static final int TYPE_WAND_ABZWEIG_LINKS_LINKS = 45;
    public static final int TYPE_WAND_ABZWEIG_LINKS_OBEN = 46;
    public static final int TYPE_WAND_ABZWEIG_LINKS_UNTEN = 47;
    
    
}
