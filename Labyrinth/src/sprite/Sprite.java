package sprite;


import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.Point;



public abstract class Sprite {
    protected Point position;
    protected int type;
    public Sprite(int x, int y){
        position = new Point(x, y);
    }
    public Point getPosition(){
        return position;
    }
    
    public int getType(){
        return type;
    }
    public abstract BufferedImage gibAktuellesBild();
    public abstract void zeichne(Graphics g2d, int size, Pictures p);
    
}
