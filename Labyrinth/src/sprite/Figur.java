package sprite;


import java.awt.Graphics;
import java.awt.image.BufferedImage;



public abstract class Figur extends Sprite{
    
    protected int sichtweite;
    protected int ausrichtung;

    public Figur(int x, int y) {
        super(x, y);
    }
    public abstract void bewege();
    public abstract void aendereAusrichtung(int pAusrichtung);
    public boolean hatGleicheXY(int pX, int pY){
        return position.x == pX && position.y == pY;
    }

    @Override
    public BufferedImage gibAktuellesBild() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void zeichne(Graphics g2d, int size, Pictures p) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
