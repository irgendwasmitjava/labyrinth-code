/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainframe;

import java.awt.Graphics;


public class Fakel extends CTSprite{
    
    public static final int POS_X_FAKEL_0 = 1;
    public static final int POS_Y_FAKEL_0 = 20;
    
    public static final int POS_X_FAKEL_1 = 130;
    public static final int POS_Y_FAKEL_1 = 15;
    
    public static final int POS_X_FAKEL_2 = 259;
    public static final int POS_Y_FAKEL_2 = 10;
   
    public static final int POS_X_FAKEL_3 = 250;
    public static final int POS_Y_FAKEL_3 = 88;
    
    public static final int POS_X_FAKEL_4 = 174;
    public static final int POS_Y_FAKEL_4 = 140;
    
    public static final int POS_X_FAKEL_5 = 75;
    public static final int POS_Y_FAKEL_5 = 170;
    
    public static final int POS_X_FAKEL_6 = -1;
    public static final int POS_Y_FAKEL_6 = 245;
    
    public static final int POS_X_FAKEL_7 = 145;
    public static final int POS_Y_FAKEL_7 = 260;
    
    public static final int POS_X_FAKEL_8 = 218;
    public static final int POS_Y_FAKEL_8 = 215;
    
    public static final int POS_X_FAKEL_9 = 325;
    public static final int POS_Y_FAKEL_9 = 215;
    
    public static final int POS_X_FAKEL_10 = 265;
    public static final int POS_Y_FAKEL_10 = 310;
    
    public static final int POS_X_FAKEL_11 = 204;
    public static final int POS_Y_FAKEL_11 = 322;
    
    public static final int POS_X_FAKEL_12 = 130;
    public static final int POS_Y_FAKEL_12 = 340;
    
    public static final int POS_X_FAKEL_13 = 41;
    public static final int POS_Y_FAKEL_13 = 338;
    
    public static final int SIZE_0_FAKEL = 74;
    public static final int SIZE_1_FAKEL = 64;
    public static final int SIZE_2_FAKEL = 50;
    public static final int SIZE_3_FAKEL = 58;
    public static final int SIZE_4_FAKEL = 58;
    public static final int SIZE_5_FAKEL = 58;
    public static final int SIZE_6_FAKEL = 58;
    public static final int SIZE_7_FAKEL = 52;
    public static final int SIZE_8_FAKEL = 58;
    public static final int SIZE_9_FAKEL = 64;
    public static final int SIZE_10_FAKEL = 48;
    public static final int SIZE_11_FAKEL = 48;
    public static final int SIZE_12_FAKEL = 45;
    public static final int SIZE_13_FAKEL = 60;
    
    public static final int FAKEL_0 = 0;
    public static final int FAKEL_1 = 1;
    public static final int FAKEL_2 = 2;
    public static final int FAKEL_3 = 3;
    public static final int FAKEL_4 = 4;
    public static final int FAKEL_5 = 5;
    public static final int FAKEL_6 = 6;
    public static final int FAKEL_7 = 7;
    public static final int FAKEL_8 = 8;
    public static final int FAKEL_9 = 9;
    public static final int FAKEL_10 = 10;
    public static final int FAKEL_11 = 11;
    public static final int FAKEL_12 = 12;
    public static final int FAKEL_13 = 13;
    
    public static final int FAKEL_COUNT_MIN = 0;
    public static final int FAKEL_COUNT_MAX = 13;
    
    
    public int fakel_count = FAKEL_1;
    
    

    public void zeichneSpirtes(Graphics g) {

       g.drawImage(fakel_nicht, POS_X_FAKEL_0, POS_Y_FAKEL_0, SIZE_0_FAKEL, SIZE_0_FAKEL, null);
       g.drawImage(fakel_nicht, POS_X_FAKEL_1, POS_Y_FAKEL_1, SIZE_1_FAKEL, SIZE_1_FAKEL, null);
       g.drawImage(fakel_nicht, POS_X_FAKEL_2, POS_Y_FAKEL_2, SIZE_2_FAKEL, SIZE_2_FAKEL, null);
       g.drawImage(fakel_nicht, POS_X_FAKEL_3, POS_Y_FAKEL_3, SIZE_3_FAKEL, SIZE_3_FAKEL, null);
       g.drawImage(fakel_nicht, POS_X_FAKEL_4, POS_Y_FAKEL_4, SIZE_4_FAKEL, SIZE_4_FAKEL, null);
       g.drawImage(fakel_nicht, POS_X_FAKEL_5, POS_Y_FAKEL_5, SIZE_5_FAKEL, SIZE_5_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_6, POS_Y_FAKEL_6, SIZE_6_FAKEL, SIZE_6_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_7, POS_Y_FAKEL_7, SIZE_7_FAKEL, SIZE_7_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_8, POS_Y_FAKEL_8, SIZE_8_FAKEL, SIZE_8_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_9, POS_Y_FAKEL_9, SIZE_9_FAKEL, SIZE_9_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_10, POS_Y_FAKEL_10, SIZE_10_FAKEL, SIZE_10_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_11, POS_Y_FAKEL_11, SIZE_11_FAKEL, SIZE_11_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_12, POS_Y_FAKEL_12, SIZE_12_FAKEL, SIZE_12_FAKEL, null);
        g.drawImage(fakel_nicht, POS_X_FAKEL_13, POS_Y_FAKEL_13, SIZE_13_FAKEL, SIZE_13_FAKEL, null);

        //       g.drawImage(fakel_aus, POS_X_FAKEL_0, POS_Y_FAKEL_0, SIZE_0_FAKEL, SIZE_0_FAKEL, null); 
        g.drawImage(fakel_aus, POS_X_FAKEL_0, POS_Y_FAKEL_0, SIZE_0_FAKEL, SIZE_0_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_1, POS_Y_FAKEL_1, SIZE_1_FAKEL, SIZE_1_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_2, POS_Y_FAKEL_2, SIZE_2_FAKEL, SIZE_2_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_3, POS_Y_FAKEL_3, SIZE_3_FAKEL, SIZE_3_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_4, POS_Y_FAKEL_4, SIZE_4_FAKEL, SIZE_4_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_5, POS_Y_FAKEL_5, SIZE_5_FAKEL, SIZE_5_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_6, POS_Y_FAKEL_6, SIZE_6_FAKEL, SIZE_6_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_7, POS_Y_FAKEL_7, SIZE_7_FAKEL, SIZE_7_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_8, POS_Y_FAKEL_8, SIZE_8_FAKEL, SIZE_8_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_9, POS_Y_FAKEL_9, SIZE_9_FAKEL, SIZE_9_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_10, POS_Y_FAKEL_10, SIZE_10_FAKEL, SIZE_10_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_11, POS_Y_FAKEL_11, SIZE_11_FAKEL, SIZE_11_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_12, POS_Y_FAKEL_12, SIZE_12_FAKEL, SIZE_12_FAKEL, null);
        g.drawImage(fakel_aus, POS_X_FAKEL_13, POS_Y_FAKEL_13, SIZE_13_FAKEL, SIZE_13_FAKEL, null);
        
        switch (fakel_count) {
            case FAKEL_0:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_0, POS_Y_FAKEL_0, SIZE_0_FAKEL, SIZE_0_FAKEL, null);

                break;
            case FAKEL_1:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_1, POS_Y_FAKEL_1, SIZE_1_FAKEL, SIZE_1_FAKEL, null);
                break;
            case FAKEL_2:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_2, POS_Y_FAKEL_2, SIZE_2_FAKEL, SIZE_2_FAKEL, null);
                break;
            case FAKEL_3:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_3, POS_Y_FAKEL_3, SIZE_3_FAKEL, SIZE_3_FAKEL, null);
                break;
            case FAKEL_4:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_4, POS_Y_FAKEL_4, SIZE_4_FAKEL, SIZE_4_FAKEL, null);
                break;
            case FAKEL_5:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_5, POS_Y_FAKEL_5, SIZE_5_FAKEL, SIZE_5_FAKEL, null);
                break;
            case FAKEL_6:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_6, POS_Y_FAKEL_6, SIZE_6_FAKEL, SIZE_6_FAKEL, null);
                break;
            case FAKEL_7:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_7, POS_Y_FAKEL_7, SIZE_7_FAKEL, SIZE_7_FAKEL, null);
                break;
            case FAKEL_8:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_8, POS_Y_FAKEL_8, SIZE_8_FAKEL, SIZE_8_FAKEL, null);
                break;
            case FAKEL_9:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_9, POS_Y_FAKEL_9, SIZE_9_FAKEL, SIZE_9_FAKEL, null);
                break;
            case FAKEL_10:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_10, POS_Y_FAKEL_10, SIZE_10_FAKEL, SIZE_10_FAKEL, null);
                break;
            case FAKEL_11:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_11, POS_Y_FAKEL_11, SIZE_11_FAKEL, SIZE_11_FAKEL, null);
                break;
            case FAKEL_12:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_12, POS_Y_FAKEL_12, SIZE_12_FAKEL, SIZE_12_FAKEL, null);
                break;
            case FAKEL_13:
                g.drawImage(fakel_an[currentpic], POS_X_FAKEL_13, POS_Y_FAKEL_13, SIZE_13_FAKEL, SIZE_13_FAKEL, null);
                break;
        }

    }

}
