
package mainframe;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.Timer;


public abstract class  CTSprite {

public static final int MENÜ_BILDER_ANZ = 8;    
public static final int LEVELMENÜ_FAKEL_BILD_ANZ = 12;
public static final int POS_X_LEVEL_SCHILD = 180;
public static final int POS_Y_LEVEL_SCHILD = 150;
public static final int SIZE_XY_MENÜ = 400;
public static final int SIZE_XY_SCHILD = 64;
public static final int SIZE_XY_SCHILD_EDITOR = 84;
public static final int POS_X_EXIT_SCHILD = 160;
public static final int POS_Y_EXIT_SCHILD = 110;
public static final int POS_X_EDITOR_SCHILD = 145;
public static final int POS_Y_EDITOR_SCHILD = 215;
public static final int LEVEL_HIGHLIGHT = 1;
public static final int EXIT_HIGHLIGHT = 2;
public static final int EDITOR_HIGHLIGHT = 0;
public static final int HIGHLIGHT_COUNT_MAX = 2;
public static final int HIGHLIGHT_COUNT_MIN = 0;

public int highlight_count = LEVEL_HIGHLIGHT;


public int currentpic = 0;    
//private Timer t;

    BufferedImage[] menü;
    BufferedImage level_schild;
    BufferedImage exit_schild;
    BufferedImage editor_schild;
    BufferedImage exit_highlight;
    BufferedImage level_highlight;
    BufferedImage editor_highlight;
    BufferedImage levelmenü;
    BufferedImage fakel_aus;
    BufferedImage[] fakel_an;
    BufferedImage fakel_nicht;
            
    
    
    
public CTSprite (){
   
    try {
          menü = loadPics("Bilder/Menü.png", MENÜ_BILDER_ANZ);
          fakel_an = loadPics("Bilder/Fakel_AN.png", LEVELMENÜ_FAKEL_BILD_ANZ);
          File schild_level = new File("Bilder/Schild_Level.png");
          File schild_exit = new File("Bilder/Schild_Exit.png");
          File highlight_exit = new File("Bilder/Schild_Exit_Highlight.png");
          File highlight_level = new File("Bilder/Schild_Level_Highlight.png");
          File menülevel = new File("Bilder/Levelmenü.png");
          File aus_fakel = new File("Bilder/Fakel_AUS.png");
          File nicht_fakel = new File("Bilder/Fakel_NICHT.png");
          File schild_editor = new File("Bilder/Schild_Editor.png");
          File highlight_editor = new File("Bilder/Schild_Editor_Highlight.png");
          
          
          editor_schild = ImageIO.read(schild_editor);
          editor_highlight = ImageIO.read(highlight_editor);        
          level_schild = ImageIO.read(schild_level);
          level_highlight = ImageIO.read(highlight_level);
          exit_schild = ImageIO.read(schild_exit);
          exit_highlight = ImageIO.read(highlight_exit);
          levelmenü = ImageIO.read(menülevel);
          fakel_aus = ImageIO.read(aus_fakel);
          fakel_nicht = ImageIO.read(nicht_fakel);
          
          
    } catch (Exception e) {
        System.out.println("Menü Bild fehlt"); 
    }
    
}
public abstract void zeichneSpirtes(Graphics g);
        


public int doAnimaton (int pic_anz){
    
    currentpic++;
    if(currentpic >= pic_anz){
       currentpic = 0; 
    }
    return currentpic;
    
}
 private BufferedImage[] loadPics(String path, int pics) {
        BufferedImage[] anim = new BufferedImage[pics];
        BufferedImage source = null;

        File pic_url = new File(path);

        try {
            source = ImageIO.read(pic_url);
        } catch (Exception e) {
        }
        for (int i = 0; i < pics; i++) {
            anim[i] = source.getSubimage(0, i * source.getHeight() / pics, source.getWidth(), source.getHeight() / pics);
        }

        return anim;
    }
   
    
}
