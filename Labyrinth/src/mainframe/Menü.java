
package mainframe;

import java.awt.Graphics;


public class Menü extends CTSprite {
    
    
   
    public void zeichneSpirtes(Graphics g) {
        g.drawImage(menü[currentpic], 0, 0,SIZE_XY_MENÜ,SIZE_XY_MENÜ, null);
        
        g.drawImage(level_schild, POS_X_LEVEL_SCHILD, POS_Y_LEVEL_SCHILD,SIZE_XY_SCHILD,SIZE_XY_SCHILD, null);
        g.drawImage(editor_schild, POS_X_EDITOR_SCHILD, POS_Y_EDITOR_SCHILD,SIZE_XY_SCHILD_EDITOR, SIZE_XY_SCHILD_EDITOR, null);
        g.drawImage(exit_schild, POS_X_EXIT_SCHILD, POS_Y_EXIT_SCHILD, SIZE_XY_SCHILD, SIZE_XY_SCHILD, null);
        
        if(highlight_count == EXIT_HIGHLIGHT){
        g.drawImage(exit_highlight, POS_X_EXIT_SCHILD, POS_Y_EXIT_SCHILD, SIZE_XY_SCHILD, SIZE_XY_SCHILD, null);        
        }
        if(highlight_count == LEVEL_HIGHLIGHT){
        g.drawImage(level_highlight, POS_X_LEVEL_SCHILD, POS_Y_LEVEL_SCHILD, SIZE_XY_SCHILD, SIZE_XY_SCHILD, null);
        }
        if(highlight_count == EDITOR_HIGHLIGHT){
        g.drawImage(editor_highlight, POS_X_EDITOR_SCHILD, POS_Y_EDITOR_SCHILD, SIZE_XY_SCHILD_EDITOR, SIZE_XY_SCHILD_EDITOR, null);    
        }
    }
    
}
