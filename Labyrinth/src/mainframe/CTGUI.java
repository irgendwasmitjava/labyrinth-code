
package mainframe;

import spiel.SpielFenster;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.Timer;
import mapbuilder.BuilderFenster;
 
public class CTGUI extends javax.swing.JPanel {
    public static final int ZEIT = 200;
    public Timer t;
//    public CTSprite ct = new CTSprite();
    public Menü m = new Menü();
    public LevelMenü lm = new LevelMenü();
    public boolean menü = true;
    public boolean levelmenü = false;
    public int count = 0;

    public CTGUI() {
        initComponents();
        this.setFocusable(true);
        this.requestFocus();
        
        t = new Timer(ZEIT, new ActionListener() {
            
            public void actionPerformed(ActionEvent ae) {
                if(menü){
                m.doAnimaton(CTSprite.MENÜ_BILDER_ANZ);
                }
                if(levelmenü){
                lm.f.doAnimaton(CTSprite.LEVELMENÜ_FAKEL_BILD_ANZ);
                }
                repaint();
            }
        });
        t.start();
    }
    public void paintComponent(Graphics g){
       if(menü){
          m.zeichneSpirtes(g);
       }
       if(levelmenü){
          lm.zeichneSpirtes(g);
       }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_UP:
                if (menü) {
                    if (m.highlight_count != CTSprite.HIGHLIGHT_COUNT_MAX) {
                        m.highlight_count++;
                    } else {
                        m.highlight_count = CTSprite.HIGHLIGHT_COUNT_MIN;
                    }
                }
                if (levelmenü) {
                    if (lm.f.fakel_count != Fakel.FAKEL_COUNT_MAX) {
                        lm.f.fakel_count++;
                    } else {
                        lm.f.fakel_count = Fakel.FAKEL_COUNT_MIN;
                    }
                }
                break;
            case KeyEvent.VK_DOWN:
                if (menü) {
                    if (m.highlight_count != CTSprite.HIGHLIGHT_COUNT_MIN) {
                        m.highlight_count--;
                    } else {
                        m.highlight_count = CTSprite.HIGHLIGHT_COUNT_MAX;
                    }
                }
                if (levelmenü) {
                    if (lm.f.fakel_count != Fakel.FAKEL_COUNT_MIN) {
                        lm.f.fakel_count--;
                    }
                }
                break;
            case KeyEvent.VK_ENTER:

                if (menü) {
                    if (m.highlight_count == CTSprite.EXIT_HIGHLIGHT) {
                        System.exit(0);
                    }
                    if (m.highlight_count == CTSprite.LEVEL_HIGHLIGHT) {
                        levelmenü = true;
                        menü = false;
                    }
                    if (m.highlight_count == CTSprite.EDITOR_HIGHLIGHT){
                        BuilderFenster f = new BuilderFenster();
                        f.setVisible(true);
                    }
                    break;
                }
                if (levelmenü) {
//                    
//                    if(lm.f.fakel_count == Fakel.FAKEL_0 ){

//                    }
                    switch (lm.f.fakel_count) {
                        case Fakel.FAKEL_0:
                            levelmenü = false;
                            menü = true;
                            lm.f.fakel_count = Fakel.FAKEL_1;
                            break;
                        case Fakel.FAKEL_1:
                            SpielFenster sf = new SpielFenster("Level_1");
                            sf.setSize(620, 640);
                            sf.setVisible(true);
                            break;
                        case Fakel.FAKEL_2:
                            SpielFenster sf2 = new SpielFenster("Level_2");
                            sf2.setSize(620, 640);
                            sf2.setVisible(true);
                            break;
                        case Fakel.FAKEL_3:
                             SpielFenster sf3 = new SpielFenster("Level_3");
                            sf3.setSize(620, 640);
                            sf3.setVisible(true);
                            break;
                        case Fakel.FAKEL_4:
                             SpielFenster sf4 = new SpielFenster("Level_4");
                            sf4.setSize(620, 640);
                            sf4.setVisible(true);
                            break;
                        case Fakel.FAKEL_5:
                             SpielFenster sf5 = new SpielFenster("Level_5");
                            sf5.setSize(620, 640);
                            sf5.setVisible(true);
                            break;
                        case Fakel.FAKEL_6:
                             SpielFenster sf6 = new SpielFenster("Level_6");
                            sf6.setSize(620, 640);
                            sf6.setVisible(true);
                            break;
                        case Fakel.FAKEL_7: SpielFenster sf7 = new SpielFenster("Level_7");
                            sf7.setSize(620, 640);
                            sf7.setVisible(true);
                            break;
                        case Fakel.FAKEL_8:
                             SpielFenster sf8 = new SpielFenster("Level_8");
                            sf8.setSize(620, 640);
                            sf8.setVisible(true);
                            break;
                        case Fakel.FAKEL_9:
                             SpielFenster sf9 = new SpielFenster("Level_9");
                            sf9.setSize(620, 640);
                            sf9.setVisible(true);
                            break;
                        case Fakel.FAKEL_10:
                             SpielFenster sf10 = new SpielFenster("Level_10");
                            sf10.setSize(620, 640);
                            sf10.setVisible(true);
                            break;
                        case Fakel.FAKEL_11:
                             SpielFenster sf11 = new SpielFenster("Level_11");
                            sf11.setSize(620, 640);
                            sf11.setVisible(true);
                            break;
                        case Fakel.FAKEL_12:
                            SpielFenster sf12 = new SpielFenster("Level_12");
                            sf12.setSize(800,800 );
                            sf12.setVisible(true);
                            break;
                        case Fakel.FAKEL_13:
                             SpielFenster sf13 = new SpielFenster("Level_13");
                            sf13.setSize(620, 640);
                            sf13.setVisible(true);
                            break;
                    }

                }

                break;
            case KeyEvent.VK_ESCAPE:
                if (levelmenü) {
                    levelmenü = false;
                    menü = true;
                }
        }
    }//GEN-LAST:event_formKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
