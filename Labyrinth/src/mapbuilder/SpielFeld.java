/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mapbuilder;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import sprite.Pictures;


public class SpielFeld extends javax.swing.JPanel {

    private Pictures p;
    private MapBuilder mapBuilder;
    private JFrame Fenster;
    private boolean hatMapBuilder = false;
    int partWidth, partHeight;

    public SpielFeld() {
        initComponents();
        setFocusable(true);
        p = new Pictures();
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent mEvt) {
                verarbeiteMausKlick(mEvt.getX(), mEvt.getY(), mEvt.getButton());
                repaint();
            }
        });
    }

    public void verarbeiteMausKlick(int pXPos, int pYPos, int button) {
        Point index = getIndex(pXPos, pYPos);
        if (button == 1) {
            mapBuilder.setzeElement(index.x, index.y);
        } else if (button == 3) {
            mapBuilder.entferneElement(index.x, index.y);
        }

    }

    public Point getIndex(int pXPos, int pYPos) {
        Point p = new Point();
        p.x = (int) pXPos / partWidth;
        p.y = (int) pYPos / partHeight;
        return p;
    }

    public void berechneNeueGroesse() {
        if (getWidth() < 650) {
            while (getWidth() % mapBuilder.getGroesse() != 0) {
                Fenster.setSize(Fenster.getWidth() + 1, Fenster.getHeight());
                this.setSize(getWidth() + 1, getHeight());
            }
        } else {
            while (getWidth() % mapBuilder.getGroesse() != 0) {
                Fenster.setSize(Fenster.getWidth() - 1, Fenster.getHeight());
                this.setSize(getWidth() - 1, getHeight());
            }
        }
        if (getHeight() < 650) {
            while (getHeight() % mapBuilder.getGroesse() != 0) {
                Fenster.setSize(Fenster.getWidth(), Fenster.getHeight() + 1);
                this.setSize(getWidth(), getHeight() + 1);
            }
        } else {
            while (getHeight() % mapBuilder.getGroesse() != 0) {
                Fenster.setSize(Fenster.getWidth(), Fenster.getHeight() - 1);
                this.setSize(getWidth(), getHeight() - 1);
            }
        }
        partWidth = getWidth() / mapBuilder.getGroesse();
        partHeight = getHeight() / mapBuilder.getGroesse();
        repaint();
    }

    public void setMapBuilder(MapBuilder pMapBuilder) {
        mapBuilder = pMapBuilder;
        hatMapBuilder = true;
        berechneNeueGroesse();
    }

    public void setJFrame(JFrame pJFrame) {
        Fenster = pJFrame;
        setSize(600, 600);
    }

    @Override
    public void paintComponent(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());
        Graphics2D g2d = (Graphics2D) g;
        if (hatMapBuilder) {
            for (int y = 0; y < mapBuilder.getGroesse(); y++) {
                for (int x = 0; x < mapBuilder.getGroesse(); x++) {
                    switch (mapBuilder.Karte.get(y).get(x)) {
                        case MapBuilder.AUSWAHL_WEG:
                            g2d.drawImage(p.Flur, x * partWidth, y * partWidth, partWidth, partHeight, null);
                            break;
                        case MapBuilder.AUSWAHL_TUER:
                            g2d.drawImage(p.Tuer, x * partWidth, y * partWidth, partWidth, partHeight, null);
                            break;
                        case MapBuilder.AUSWAHL_NICHTS:
                            g.setColor(Color.LIGHT_GRAY);
                            g.fillRect(x * partWidth, y * partHeight, partWidth, partHeight);
                            break;
                        case MapBuilder.AUSWAHL_WAND:
                            g.setColor(Color.BLACK);
                            g.fillRect(x * partWidth, y * partHeight, partWidth, partHeight);
                            break;
                        case MapBuilder.AUSWAHL_WEG_SCHLUESSELPUNKT:
                            g2d.drawImage(p.Key, x * partWidth, y * partWidth, partWidth, partHeight, null);
                            break;
                        case MapBuilder.AUSWAHL_WEG_STARTPUNKT:
                            g.setColor(Color.GREEN);
                            g.fillRect(x * partWidth, y * partHeight, partWidth, partHeight);
                            break;
                    }
                }
            }
            for (int y = 0; y <= mapBuilder.getGroesse(); y++) {
                g.setColor(Color.BLACK);
                g.drawLine(0, y * partHeight, getWidth(), y * partHeight);
            }
            for (int x = 0; x <= mapBuilder.getGroesse(); x++) {
                g.drawLine(x * partWidth, 0, x * partWidth, getHeight());
            }

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setMinimumSize(new java.awt.Dimension(600, 600));
        setPreferredSize(new java.awt.Dimension(600, 600));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
