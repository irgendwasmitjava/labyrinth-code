package mapbuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


public class MapBuilder {

    ArrayList<ArrayList<String>> Karte;
    private String auswahl = AUSWAHL_WAND;
    public static final String AUSWAHL_NICHTS = "n";
    public static final String AUSWAHL_WAND = "a";
    public static final String AUSWAHL_WEG = "b";
    public static final String AUSWAHL_WEG_SCHLUESSELPUNKT = "c";
    public static final String AUSWAHL_WEG_STARTPUNKT = "d";
    public static final String AUSWAHL_TUER = "e";
    private boolean hatStartPunkt = false;
    private boolean hatEndPunkt = false;
    private Auswahl dieAuswahl;

    public MapBuilder() {
        Karte = new ArrayList<>();
        for (int x = 0; x < 15; x++) {
            Karte.add(new ArrayList<>());
        }
        for (int y = 0; y < Karte.size(); y++) {
            for (int x = 0; x < 15; x++) {
//                 String s = "(" + Integer.toString(y) + " / " + Integer.toString(x) + ") ";
                Karte.get(x).add(AUSWAHL_NICHTS);
            }

        }
    }

    public void setAuswahl(Auswahl pAuswahl) {
        dieAuswahl = pAuswahl;
    }

    // Methoden fuer das Auswahlpanel 
    public void vergroessereSpielfeld() {
        if (Karte.size() < 30) {
            addRow();
            addColumn();
        }
    }

    public void verkleinereSpielfeld() {
        if (Karte.size() > 15) {
            removeRow();
            removeColumn();
        }

    }

    private void addRow() {
        Karte.add(new ArrayList<>());
        for (int x = 0; x < Karte.get(0).size(); x++) {
//            String s = "(" + Integer.toString(x) + " / " + Integer.toString(Karte.size()) + ") ";
            Karte.get(Karte.size() - 1).add(AUSWAHL_NICHTS);
        }

    }

    private void removeRow() {
        Karte.remove(Karte.size() - 1);
    }

    private void addColumn() {
        for (int y = 0; y < Karte.size(); y++) {
//            String s = "(" + Integer.toString(Karte.size()) + " / " + Integer.toString(y) + ") ";
            Karte.get(y).add(AUSWAHL_NICHTS);
        }
    }

    private void removeColumn() {
        for (int y = 0; y < Karte.size(); y++) {
            Karte.get(y).remove(Karte.get(y).size() - 1);
        }
    }

    public void aendereAuswahl(String pAuswahl) {
        auswahl = pAuswahl;
    }

    // Methoden fuer das Zeichenpanel
    public void setzeElement(int indexX, int indexY) {
        if (auswahl == AUSWAHL_WEG_STARTPUNKT && hatStartPunkt) {
            return;
        }
        if (auswahl == AUSWAHL_TUER && hatEndPunkt) {
            return;
        }
        if (auswahl == AUSWAHL_WEG_STARTPUNKT && !hatStartPunkt) {
            hatStartPunkt = true;
        }
        if (auswahl == AUSWAHL_TUER && !hatEndPunkt) {
            hatEndPunkt = true;
        }
        Karte.get(indexY).set(indexX, auswahl);
    }

    public void entferneElement(int indexX, int indexY) {
        if (auswahl == AUSWAHL_WEG_STARTPUNKT) {
            hatStartPunkt = false;
        }
        if (auswahl == AUSWAHL_TUER) {
            hatEndPunkt = false;
        }
        Karte.get(indexY).set(indexX, AUSWAHL_NICHTS);
    }

    public String getAuswahl() {
        return auswahl;
    }

    public int getGroesse() {
        return Karte.size();
    }

    // Methoden um Array abzuspeichern
    public void speichereMap() {
        Properties props = System.getProperties();
        BufferedWriter bWriter;
        for (int y = 0; y < Karte.size(); y++) {
            for (int x = 0; x < Karte.get(0).size(); x++) {
                if (Karte.get(y).get(x) == "n") {
                    Karte.get(y).set(x, AUSWAHL_WAND);
                }
            }
        }
        try {
            bWriter = new BufferedWriter(new FileWriter(props.getProperty("user.dir") + File.separator + "levels" + File.separator + dieAuswahl.getMapName() + ".txt"));
            for (int y = 0; y < Karte.size(); y++) {
                String s = "";
                for (int x = 0; x < Karte.get(0).size(); x++) {
                    s += Karte.get(y).get(x);
                }
                bWriter.write(s);
                bWriter.newLine();
            }
            bWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(MapBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    // Methoden zu Testzwecken
    public void printArray() {
        System.out.println("---------------------------------------------------");
        for (int y = 0; y < Karte.size(); y++) {
            for (int x = 0; x < Karte.get(0).size(); x++) {
                System.out.print(Karte.get(y).get(x));
            }
            System.out.println(" ");
        }
        System.out.println("---------------------------------------------------");
    }
    // Methode(/n) zum Abspeichern

}
